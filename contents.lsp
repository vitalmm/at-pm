;;; 图框信息相关函数
(defun @:check-consistency (contents order / ti% tmplist)
  "检查某键值的唯一性。返回值为整数。"
  (setq tmplist '())
  (foreach ti% contents 
	   (if (= nil (member (cdr (assoc order ti%)) tmplist))
	       (setq tmplist (append tmplist (list (cdr (assoc order ti%)))))))
  (length tmplist)
  )
(defun @pm:get-frame-info (blkref)
  (append 
   (block:get-attributes blkref)
   (block:get-properties blkref)
   (list
    (cons "boxInsp" (entity:getdxf blkref 10))
    (cons "boxScale" (entity:getdxf blkref 41))
    (cons "boxRotate" (entity:getdxf blkref 50))
    (cons "layout"  (entity:getdxf blkref 410))
    (cons "ename" blkref)
    )
   ))
(defun @pm:get-prj-info-from-tukuang (/ tk)
  "选择一个图框，根据图框信息读入项目名称."
  (setq tk (car (entsel "请点选一个图框:")))
  (if (and tk
	   (= "INSERT"(entity:getdxf tk 0))
	   (equal (block:get-effectivename tk)
		  (@::get-config '@pm:tukuang)))
      (@pm:get-frame-info tk)
    ))
(defun @pm:compare-string (x y / n a b lx ly)
  (if (and (p:stringp x)(p:stringp y))
      (progn
        (setq n 0)
        (setq lx (string:to-list x "-"))
        (setq ly (string:to-list y "-"))
        (while (and (< n (length lx))
		    (< n (length ly))
		    (= (nth n lx)
		       (nth n ly)))
          (setq n (1+ n)))
        (setq a (nth n lx)
              b (nth n ly))
        (if (and a b)
            (cond ((and (string:intp a)
			(string:intp b))
		   (< (abs(atoi a))
                      (abs(atoi b))))
		  ((and (string:realp a)
			(string:realp b))
		   (< (abs(atof a))
                      (abs(atof b))))
		  ((and (string:hannumberp a)
			(string:hannumberp b))
		   (< (string:hannumber2number a)
                      (string:hannumber2number b)))
		  (t (< a b)))))))
(defun @pm:sort-by-order1 (contents order)
  (list:sort contents
	     '(lambda (x y)
		(@pm:compare-string
		 (cdr (assoc order x))
		 (cdr (assoc order y)))))
  )
(defun @pm:sort-by-order (all order / l m b c)
  "对图纸目录列表进行排序"
  (setq n (length all))
  (setq l 0)
  (setq m 1)
  (while (< l n)
    (setq b (nth l all))
    (while (< m n)
      (setq c (nth m  all))
      (if (and (last (string:to-list
		      (@:string-subst "-" "－" (cdr (assoc order c))) "-"))
	       (last (string:to-list
		      (@:string-subst "-" "－" (cdr (assoc order b))) "-" )))
	  (if (< (atoi (last (string:to-list
			      (@:string-subst "-" "－" (cdr (assoc order c))) "-")))
		 (atoi (last (string:to-list
			      (@:string-subst "-" "－" (cdr (assoc order b))) "-" ))))
              (progn
		(setq all (subst 'aa (nth l all) all))
		(setq all (subst 'bb (nth m all) all))
		(setq all (subst c 'aa all))
		(setq all (subst b 'bb all))
		(setq b c)
		)
            ))
      (setq m (1+ m))
      )
    (setq l (1+ l))
    (setq m (1+ l))
    )
  all
  )
(defun @pm:make-contents-back (s1 / ename ti% p ml-contents xmax ymin boxscale insp en0 )
  "生成图纸目录列表(图框块的所有属性)及位置比例信息(用于批量打印)"
  (setq ml-contents (mapcar '@pm:get-frame-info (pickset:to-list s1)))
  (@pm:sort-by-order1 ml-contents (@:get-config '@pm:drawing-number))
  )
(defun @pm:frames2contents (frames)
  "图框生成目录集"
  (if frames
      (@pm:make-contents-back (pickset:from-entlist frames))))
(defun @pm:make-contents (/ s1)
  (prompt "回车或右键则为所有图框")
  (if @:*auto-mode*
      (setq s1 (ssget "x" (list '(0 . "INSERT")
				(cons 2  (strcat (@:get-config '@pm:tukuang) ",`**")))))
    (progn
      (setq s1 (ssget (list '(0 . "INSERT")
			    (cons 2  (strcat (@:get-config '@pm:tukuang) ",`**")))))
      (if (null s1)
	  (setq s1 (ssget "x" (list '(0 . "INSERT")
				    (cons 2  (strcat (@:get-config '@pm:tukuang) ",`**"))))))))
  (if (and (= s1 nil)
	   (null @:*auto-mode*))
      (progn
	(@:alert (strcat "未选中任何图框，请检查配置文件中你的图框块名是 "
		       (@:get-config '@pm:tukuang)))
	(@:edit-config)))
  (@pm:make-contents-back s1))
(defun @pm:make-all-contents (/ s1)
  "生成图纸目录列表(图框块的所有属性)及位置比例信息(用于批量打印)"
  (setq s1 (ssget "x" (list '(0 . "INSERT")
			    (cons 2  (strcat (@:get-config '@pm:tukuang) ",`**")))))
  (@pm:make-contents-back s1)
  )

;; 图别专业
(defun @pm:get-tubie (content / tubie)
  "从图框信息中获取图别，"
  (if (or (setq tubie (cdr (assoc "图别" content)))
	  (setq tubie (cdr (assoc "专业" content)))
	  (setq tubie (cdr (assoc (@:get-config '@pm:profession-name) content)))
	  )
      (if (or 
	   (member tubie (string:to-list (@:get-config '@pm:ml-order) ";" ))
	   (member tubie (string:to-list (@:get-config '@pm:ml-order-preliminary) ";" ))
	   (member tubie (string:to-list (@:get-config '@pm:ml-order-rf) ";")))
	  tubie
	(progn
	  (@:log "WARN" "图别/专业名称不在目录类别表内。")
	  tubie
	  ))
      ;; 从图名中取图别
      (if (cdr (assoc (@:get-config '@pm:drawing-number) content))
	  (progn
	    (setq tubie (car (string:to-list
			      (@:string-subst "-" "－"
					      (cdr (assoc (@:get-config '@pm:drawing-number)  content)))
			      "-")))
	    (if (or 
		 (member tubie (string:to-list (@:get-config '@pm:ml-order) ";" ))
		 (member tubie (string:to-list (@:get-config '@pm:ml-order-preliminary) ";" ))
		 (member tubie (string:to-list (@:get-config '@pm:ml-order-rf) ";")))
		tubie
		""))
	  "")))
(defun @pm:have-tubie-attribute (content)
  "测试是否有图别/专业属性。"
  (or (assoc "图别" content)
      (assoc "专业" content)
      (assoc (@:get-config '@pm:profession-name) content))
  )
;; 图号
(defun @pm:set-tuhao (ss-list / num1 start )
  (@:help '("按设定的排列顺序重置框的图纸编号"))
  (if @:*auto-mode*
      (setq start 1)
    (setq start (getint "请输入图纸起始编号<1>:")))
  (if (null start) (setq start 1))
  (setq num1 0)
  (setq sub-num 0)
  ;;(print (length ss-list))
  (foreach en0 ss-list
	   ;;写入专业图别
	   (cond
	    ((or (p:enamep en0)
		 (and (listp en0)
		      (= 1 (length en0))))
	     (if (listp en0)(setq en0 (car en0)))
	     (if (= "" (@pm:get-tubie (@pm:get-frame-info en0)))
		 (block:set-attributes en0
				       (list (cons (@::get-config '@pm:profession-name) (@::get-config '@pm:my-profession)))))
	     (block:set-attributes
	      en0
	      (list (cons (@:get-config '@pm:drawing-number)
			  (strcat
			   (cond
			    ((/= "" (@:get-config '@pm:drawing-number-prefix))
			     (strcat (@:get-config '@pm:drawing-number-prefix) "-"))
			    ((and (@pm:get-tubie (block:get-attributes en0))
				  (/= "" (@pm:get-tubie (block:get-attributes en0)))
				  (null (member (cdr (assoc (@::get-config '@pm:profession-name) (block:get-attributes en0)))
						(append  (string:to-list (@:get-config '@pm:ml-order) ";" )
							 (string:to-list (@:get-config '@pm:ml-order-preliminary) ";" )
							 (string:to-list (@:get-config '@pm:ml-order-rf) ";"))))
				  )
			     (strcat (@pm:get-tubie (block:get-attributes en0)) "-"))
			    ((assoc (@::get-config '@pm:profession-name) (block:get-attributes en0)) "")
			    ((/= "" (@:get-config '@pm:my-profession))
			     (strcat (@:get-config '@pm:my-profession) "-"))
			    (t "")
			    )
			   ;; (if (< (+ num1 start) 10) "0" "")
			   (string:number-format (itoa (+ num1 start))
						 (@::get-config '@pm:drawing-number-bits)
						 0
						 "0")
			   ))
		    )))
	    ((and (listp en0)
		  (< 1 (length en0)))
	     (setq sub-num 0)
	     (foreach
	      en0% en0
	      (if (= ""(@pm:get-tubie (@pm:get-frame-info en0%)))
		  (block:set-attributes en0%
					(list (cons (@::get-config '@pm:profession-name) (@::get-config '@pm:my-profession)))))
	      
	      (block:set-attributes
	       en0%
	       (list (cons (@:get-config '@pm:drawing-number)
			   (strcat
			    (cond
			      ((/= "" (@:get-config '@pm:drawing-number-prefix))
			       (strcat (@:get-config '@pm:drawing-number-prefix) "-"))
			      ((and (@pm:get-tubie (block:get-attributes en0%))
				    (/= "" (@pm:get-tubie (block:get-attributes en0%)))
				    (null (member (cdr (assoc (@::get-config '@pm:profession-name) (block:get-attributes en0%)))
						  (append  (string:to-list (@:get-config '@pm:ml-order) ";" )
							   (string:to-list (@:get-config '@pm:ml-order-preliminary) ";" )
							   (string:to-list (@:get-config '@pm:ml-order-rf) ";"))))
				    )
			       (strcat (@pm:get-tubie (block:get-attributes en0%)) "-"))
			      ((assoc (@::get-config '@pm:profession-name) (block:get-attributes en0%)) "")
			      ((/= "" (@:get-config '@pm:my-profession))
			       (strcat (@:get-config '@pm:my-profession) "-"))
			      (t "")
			      )
			    ;; (if (< (+ num1 start) 10) "0" "")
			    (string:number-format (itoa (+ num1 start))
						  (@::get-config '@pm:drawing-number-bits)
						  0
						  "0")
				    "-"
				    (string:number-format (itoa (setq sub-num (1+ sub-num)))
							  (@::get-config '@pm:drawing-number-bits)
							  0
							  "0")
				    ))
		     )))
	     
	     ))
	   (setq num1 (1+ num1)))
  )
;; 图名
(defun @pm:get-tuming (content% / tuming)
  (@:to-string
   (string:from-list
    (vl-remove
     nil
     (list
      (if (null (member (@:get-config '@pm:drawing-name)'("图名" "图名第一行")))
	  (text:remove-fmt (cdr (assoc (@:get-config '@pm:drawing-name) content%))))
      (text:remove-fmt (cdr (assoc "图名" content%)))
      (text:remove-fmt (cdr (assoc "图名第一行" content%)))
      (text:remove-fmt (cdr (assoc "图名第二行" content%)))))
    " ")))
;; 图幅
(defun @pm:get-map-sheet (content% / height width tk% paper i% ps)
  "取图幅尺寸"
  (cond
   ((member (cdr (assoc "map-sheet" content%)) (mapcar 'car @:paper-size))
    (cdr (assoc "map-sheet" content%)))
   ((member (cdr (assoc "可见性" content%)) (mapcar 'car @:paper-size))
    (cdr (assoc "可见性" content%)))
   ((member (cdr (assoc "图幅" content%)) (mapcar 'car @:paper-size))
    (cdr (assoc "图幅" content%)))
   ((member (entity:getdxf (cdr (assoc "ename" content%)) 2) (mapcar 'car @:paper-size))
    ;;块名为图幅
    (entity:getdxf (cdr (assoc "ename" content%)) 2)
    )
   (t ;; 非标图框处理
    ;; 取宽高及比例
    (setq tk% (entity:getbox (cdr (assoc "ename" content%)) 0))
    (setq height (abs(/(- (cadadr tk%)(cadar tk%)) (cdr (assoc "boxScale" content%))(@:get-config '@::draw-scale))))
    (setq width (abs (/(- (caadr tk%)(caar tk%))(cdr (assoc "boxScale" content%)) (@:get-config '@::draw-scale))))
    ;;(print height)(print width)
    (setq paper "非标图框")
    (setq i -1)
    (while (and (< (setq i (1+ i))(length @:paper-size))
		(or 
		 (>= height (+ (caddr (nth i @:paper-size)) 2))
		 (>= width (+ (cadr (nth i @:paper-size)) 2)))))
    (if (< i (length @:paper-size))
	(setq paper  (car (nth i @:paper-size)))
      ;; 按合适的长宽比例缩放图纸
      (progn
	(setq i -1)
	(setq ps (vl-sort @:paper-size '(lambda (x y)
					  (or (and (eq (/ (cadr x)(caddr x)) (/ (cadr y)(caddr y)) 0.001)
						   (> (caddr x)(caddr y)))
					      (< (/ (cadr x)(caddr x))(/ (cadr y)(caddr y)))))))
	
	(while (< (/ width height) (/ (cadr (nth (setq i (1+ i)) ps)) (caddr (nth i ps)))))
	(if (< i (length @:paper-size))
	    (setq paper (car (nth i ps)))
	  (setq paper (car (last ps)))
	  )
	))
    paper)))
;; 目录
(defun @pm:menu-make-mulu ()
  (@:help (strcat "按选中的图框生成图纸目录。目前不支持一图多工程方式生成目录，"
		  "只支持单一子项工程生成目录。\n      在生成目录前检查目录中的"
		  "工程名称、建设单位等是否唯一，并给出提示。\n      "
		  "目录生成成功后存放在设置的工程输出目录下对应的子项工程下，"
		  "用于生成全部工程的目录（目录成图）。"))
  (@pm:make-mulu-csv (list:flatten(@pm:get-frames))))
(defun @pm:mulu-path (prj-info / mulu-path)
  (setq mulu-path (@:mkdir
		   (append (@:path (@:get-config '@pm:projects-output))
			   (mapcar (function(lambda(x)(text:remove-fmt(cdr (assoc x prj-info)))))
				   (string:to-list (@:get-config '@pm:project-tree) ";")))))
  ;;(print dir-path)
  (if (findfile (@:get-config '@pm:projects-yun))
      (setq mulu-path (@:mkdir
		       (append (@:path (@:get-config '@pm:projects-yun))
			       (mapcar (function(lambda(x)(text:remove-fmt(cdr (assoc x prj-info)))))
					     (string:to-list (@:get-config '@pm:project-tree) ";")))))
      )
  mulu-path
  )
(defun @pm:make-mulu-csv (frames / ti% contents content% mlstr jsdw gcmc date stage banci
			  ml-file dir-path fp)
  (setq mlstr "")
  (setq contents (@pm:frames2contents frames))
  (if (null contents)
      (progn
	(princ "未选中任何图框。")(exit)))
  ;;(princ contents)
  ;; 检查建设单位，工程名称，日期，阶段和版次等
  (foreach ti%  (string:to-list (@:get-config '@pm:consistency) ";")
	   (if (< 1 (@:check-consistency contents ti%))
	       (progn
		 (setq mlstr (strcat mlstr  "!!! " ti% "不一致。自行判断是否有误\n"))
		 )))
  ;; (print "check-consistency ok!")
  (if (/= mlstr "") 
      (@:alert mlstr)
    (progn
      ;; 生成目录文件
      (setq ti% 0)
      (setq ml-file nil)
      (foreach content% contents
	       (setq dir-path (@pm:mulu-path content%))

	       (if (null (@pm:get-tubie content%)) (progn (@:alert "图别有误,请检查图框中的图号及图别。")(exit)))
	       (setq ml-file (strcat dir-path "\\" (@pm:get-tubie content%) ".csv"))
	       (print ml-file)
	       
	       (if (= ti% 0)
		   (setq fp (open ml-file "w"))
		 (setq fp (open ml-file "a")))
	       (write-line (strcat 
			    (if (and (@pm:have-tubie-attribute content%)
				     (= 1 (@:get-config '@pm:prefix-profession))
				     (/= "" (@pm:get-tubie content%))
				     (/= (@pm:get-tubie content%)
					 (cdr (assoc (@:get-config '@pm:drawing-number) content%)))
				     )
				(strcat (@pm:get-tubie content%) "-" (cdr (assoc (@:get-config '@pm:drawing-number) content%)))
			      (cdr (assoc (@:get-config '@pm:drawing-number) content%)))
			    ","(@pm:get-tuming content%)","
			    (@pm:get-map-sheet  content%))
			   fp)
	       
	       (close fp)(setq ti% (1+ ti%)))
      
      (if ml-file
	  (progn 
	    (@:alert (strcat "目录文件:" ml-file
			   "\n      可以用Excel或记事本打开。\n\n"
			   (@:get-file-contents ml-file)))
	    ;; 复制到云
	    
	    )
	(@:alert "图框中没有设置正确的工程信息。")
	)))
  
  (princ))
(defun @pm:get-mulu-path (/ prj-info)
  (setq prj-info (@pm:get-prj-info-from-tukuang))
  (if prj-info
      (setq dir-path (@pm:mulu-path prj-info ))))
(defun @pm:insert-mulu-to-dwg (/ *error* mulu-path fp% mulu% i% insert-point% page-number% zhuanye ml-ruler ml-order prj-info)
  (defun *error* (msg)
    (pop-var)
    (princ msg)
    (princ)
    )
  (@:help (strcat "选择一个图框，根据图框的工程信息在DWG中插入图纸目录。\n"
		  "      目录内容为各专业在工程输出目录下生成的各自的专业目录。\n"
		  "操作步骤：\n"
		  "      1. 选择一个图框。\n"
		  "      2.点击一个插入点。\n然后系统会读取工程输出目录下的目录文件"
		  "插入目录到当前图形中。"))
  (setq prj-info  (@pm:get-prj-info-from-tukuang))
  (if prj-info
      (progn
	(setq mulu-path(@pm:mulu-path prj-info))
	(@:prompt mulu-path)
	)
      (setq mulu-path nil))
  (setq liebiao nil)
  (cond
   ((member (@pm:get-tubie prj-info) (string:to-list (@:get-config '@pm:ml-order) ";"))
    (setq liebiao (string:to-list (@:get-config '@pm:ml-order) ";")))
   ((member (@pm:get-tubie prj-info) (string:to-list (@:get-config '@pm:ml-order-rf) ";"))
    (setq liebiao (string:to-list (@:get-config '@pm:ml-order-rf) ";")))
   ((member (@pm:get-tubie prj-info) (string:to-list (@:get-config '@pm:ml-order-preliminary) ";"))
    (setq liebiao (string:to-list (@:get-config '@pm:ml-order-preliminary) ";")))
   ((null liebiao)
    (alert (strcat
	    (if (/= "" (@pm:get-tubie prj-info))
		(@pm:get-tubie prj-info)
	      (progn (car (string:to-list (cdr (assoc "图号" prj-info)) "-"))))
	    " 不符合目录汇总的专业要求。请设置配置项 @pm:ml-order 的值。"))
    (@pm:setup))
   )
  (if mulu-path
      (progn
	(setq ml-ruler (mapcar 'atoi (string:to-list (@:get-config '@pm:ml-ruler) ";")))
	(setq insert-point% (getpoint "请输入目录插入起始点："))
	(setq i% 0)(setq page-number% 1)
	(push-var nil)
	(setvar "osmode" 16384)
	(foreach zhuanye liebiao
		 (if (findfile (strcat mulu-path "\\" zhuanye ".csv"))
		     (progn
		       (setq fp% (open (strcat mulu-path "\\" zhuanye ".csv") "r"))
		       (while (setq mulu-s%  (read-line fp%))
			 (print mulu-path)(print zhuanye)(print mulu-s%)
			 (setq mulu% (string:to-list mulu-s% ","))
			 (if (= 3 (length mulu%))
			     (progn
			       (entity:make-text (itoa page-number%)
						 (list (+ (nth 0 insert-point%)(nth 1 ml-ruler) )
						       (- (nth 1 insert-point%)
							  (* i% (nth 0 ml-ruler)) -120)
						       0) 350 0 0.72 0 11)
			       (entity:make-text (nth 0 mulu%)
						 (list (+ (nth 0 insert-point%) (nth 2 ml-ruler))
						       (- (nth 1 insert-point%)
							  (* i% (nth 0 ml-ruler)) -120)
						       0) 350 0 0.72 0 11)
			       (entity:make-text (nth 1 mulu%)
						 (list (+ (nth 0 insert-point%) (nth 3 ml-ruler))
						       (- (nth 1 insert-point%)
							  (* i% (nth 0 ml-ruler)) -120)
						       0) 350 0 0.72 0 11)
			       (entity:make-text (nth 2 mulu%)
						 (list (+ (nth 0 insert-point%) (nth 4 ml-ruler))
						       (- (nth 1 insert-point%)
							  (* i% (nth 0 ml-ruler)) -120)
						       0) 350 0 0.72 0 11)
			       (setq page-number% (1+ page-number%))))
			 (setq i% (1+ i%)))
		       (close fp%)
		       (setq i% (1+ i%))
		       )))
	(pop-var)
	)
    (@:alert "未选中工程图框。"))
  (princ))
(defun @pm:make-total-contents ()
  (@:help "目录汇总：\n将所有专业的图纸目录进行汇总。\n生成完整的目录。"))
