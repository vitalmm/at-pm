(defun string:from-lst	(lst Separator)
  "列表转成字符串"
  (if	(cdr lst)
	(strcat (car lst) Separator (string:from-lst (cdr lst) Separator))
	(car lst)
	)
  )

(defun string:to-lst	(str Separator / pos)
  "字符串转成列表"
  (if	(setq pos (vl-string-search Separator str))
	(cons (substr str 1 pos)
	      (string:to-lst (substr str (+ pos 1 (strlen Separator))) Separator)
	      )
	(list str)
	)
  )

(defun @pm:saveas-dxf (file-name file-path ss)
  "save selected entity to dxf"
  (if (and (= 'str (type file-name))
	   (/= "" file-name)
	   (vl-file-directory-p file-path)
	   (> (sslength ss) 0))
      (progn
	;;(push-var nil)
	(setq file-name
	      (string:from-lst
	       (reverse (cdr (reverse (string:to-lst file-name "."))))
	       "."))
	(setvar "cmdecho" 0)
	(setvar "filedia" 0)
	(vl-cmdf "_.dxfout" (strcat file-path "\\" file-name)  "O" ss "" "16" )
	;;(pop-var)
	T)
      nil
      )
  (setvar "filedia" 1)
  )

(defun c:mj-req ()
  (@pm:saveas-dxf
   (getvar "DWGNAME")
   "C:\\2dTO3d"
   (ssget '((-4 . "<NOT") (8 . "DIM,TEMP")(-4 . "NOT>")))))

