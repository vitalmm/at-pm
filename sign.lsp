(defun @pm:attach-sign (frames / contents  boxScale boxInsp boxRotate content%  offset ss ti% en0 tk insunits)
  (@:help (strcat "将个人手写签名文件附着到图框指定位置，"
		  "      本功能需要先将手写签名文件写到标准库文件夹下。\n"
		  "      目前不支持镜像图框。"))
  (if (null (@:get-config '@pm:sign-offset))
      (setq offset '(-1000 12500))
    (setq offset (mapcar 'atof (string:parse-by-lst (@:get-config '@pm:sign-offset)'(","" "";")))))

  (if (null (findfile "packages\\at-pm\\sign.dwg"))
      (progn(@:load-module 'pkgman)
	(@:down-pkg-file  (@:uri) "at-pm/sign.dwg" "stable")
	(@:alert "正在下载签名示例，请稍候...")(sleep 5)))
  (if (findfile  "packages\\at-pm\\sign.dwg")
      (if (null (findfile (strcat (@:get-config '@pm:tuku) "\\"
				  (@:get-config '@pm:sign-dwg) ".dwg")))
	  (progn
	    (vl-file-copy (strcat @:*prefix* "packages\\at-pm\\sign.dwg")
			  (strcat (@:get-config '@pm:tuku) "\\"
				  (@:get-config '@pm:sign-dwg) ".dwg"))
	    )))
  (setq insunits (getvar "insunits"))

  (if (/= (getvar "insunitsdeftarget")
	  (getvar "insunitsdefsource"))
      (@:alert (strcat "待插入源图形的长度单位与目标图形的长度单位不一致。\n"
		     "插入的参照图形比例显示可能不一致。\n"
		     " 请输入 insunitsdeftarget 及 insunitsdevsource 调整。")))
  ;; (setvar "insunitstarget" (getvar "insunitssource"))

  (if (findfile (strcat (@:get-config '@pm:tuku) "\\"
			(@:get-config '@pm:sign-dwg) ".dwg"))
      (progn
	(setq contents (@pm:frames2contents frames))
	(setvar "cmdecho" 0)
	(foreach content% contents
		 (setq boxScale (float (cdr (assoc "boxScale" content%))))
		 (setq boxRotate (float (cdr (assoc "boxRotate" content%))))
		 (setq boxInsp (cdr (assoc "boxInsp" content%)))
		 ;; 矢量运算
		 (setq ins-point
		       (m:coordinate boxInsp
				     (m:coordinate-scale
				      (m:coordinate-rotate offset boxRotate) boxScale)))
		 ;;浩辰，中望及低版本CAD用块参照
		 (cond
		  ((or is-gcad is-zwcad (< (@:acadver) 18))
		   (block:insert (@:get-config '@pm:sign-dwg)
				 (system:dir (@:get-config '@pm:tuku))
				 ins-point
				 boxRotate
				 boxscale))
		  ((>= (@:acadver) 18)
		   (command "-attach" (strcat (system:dir(@:get-config '@pm:tuku))
					      (@:get-config '@pm:sign-dwg) ".dwg")
			    "A" ins-point boxScale boxScale (angtos boxRotate 0 0) )))
		 )
	(setvar "cmdecho" 1)
	))
  ;;变更签名
  (foreach tk '("图框-变更"  "图框-工程处理方案")
	   (setq ss (ssget "x" (list '(0 . "insert")  (cons 2  tk))))
	   (setq ti% 0)
	   (if (/= ss nil)
	       (progn
		 (while
		     (<= ti% (- (sslength ss) 1))
		   (setq en0 (ssname ss ti%))
		   (setq boxScale (cdr (assoc 41 (entget en0)))
			 boxInsp (cdr (assoc 10 (entget en0)))
			 boxRotate (cdr (assoc 50 (entget en0))))
		   (cond
		    ((or is-gcad is-zwcad (< (@:acadver) 18))
		     (block:insert "sign-bg"
				   (system:dir (@:get-config '@pm:tuku))
				   ins-point
				   boxRotate
				   boxscale))
		    ((>= (@:acadver) 18)
		     (command "-attach" (strcat (system:dir(@:get-config '@pm:tuku))
						"sign-bg.dwg")
			      "A" ins-point boxScale boxScale (angtos boxRotate 0 0) )))
		   (setq ti% (+ 1 ti%))))))
  (setvar "insunits" insunits)

  )
(defun @pm:remove-sign ()
  (mapcar 'entdel (pickset:to-list(ssget "x" (list '(0 . "insert")(cons 2 (@:get-config '@pm:sign-dwg)))))) 
  )
