;;(if (= (@:get-config '@pm:tukuang) "北华动态属性块")
;;    (@:add-menu "大绘图" "修正北华旧图框" "(@:move-old-tk)"))
(defun @:move-old-tk ( / s1 ti% en0 e vn0 boxScale boxInsp boxRotate content% contents offset)
  (@:help "私有代码：重定义旧图框，并修正图框位置。\n")
  (setq offset '(413461.497 -89538.921))
  (setvar "attreq" 0)
  (command "-insert" (strcat "北华动态属性块="(@:get-config '@pm:tuku) "\\"(@:get-config '@pm:tukuang)".dwg") '(0 0 0) "1" "" 0)
  (setvar "attreq" 1)
  (setq s1 (ssget "x" '((0 . "INSERT"))))
  (setq ti% 0)
  (if (/= s1 nil)
      (progn
	(while
	    (<= ti% (- (sslength s1) 1))
	  (setq en0 (ssname s1 ti%))
	  (if (eq (block:get-effectivename en0) "北华动态属性块")
	      (progn
		(setq e (entget en0))
		(setq boxScale (cdr (assoc 41 e))
		      boxInsp (cdr (assoc 10 e))
		      boxRotate (cdr (assoc 50 e)))
		(setq ins-point
		      (m:coordinate boxInsp
				    (m:coordinate-scale
				     (m:coordinate-rotate offset boxRotate) boxScale)))
		(setq e (subst (cons 10 ins-point) (assoc 10 e) e))
		(entmod e)
		))
	  (setq ti% (+ 1 ti%))
	  )
	))
  (entdel (entlast))
  (command "attsync" "n" "北华动态属性块")
  )
