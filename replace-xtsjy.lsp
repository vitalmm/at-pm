(defun replace-xtsjy ()
  (@::help "替换邢台市设计院图框")
  (setq tks (pickset:to-list (ssget '((0 . "INSERT")(2 . "A1*,A2*")))))
  (foreach
   tk tks
   (setq pt-ins (cadr (apply 'point:rec-2pt->4pt (entity:getbox tk 0))))
   (setq new-tk
	 (block:insert
	  (@::get-config '@pm:tukuang)
	  "D:\\Design\\standard\\"
	  pt-ins
	  (entity:getdxf tk 50)
	  (entity:getdxf tk 41)))
   (block:set-dynprop new-tk "map-sheet" (entity:getdxf tk 2))
   ;;复制属性值
   (block:set-attributes new-tk (block:get-attributes tk))
   )
  )
