(defun @pm:switch-frame (content%)
  (setq content% (nth ti% s1))
  (setq tufu (@pm:get-map-sheet content%))
  (setq boxScale (float (cdr (assoc "boxScale" content%))))
  (setq boxRotate (float (cdr (assoc "boxRotate" content%))))
  
  (setq boxInsp (cdr (assoc "boxInsp" content%)))
  (if (and (setq tufu-number (cdr (assoc tufu  @:paper-size)))
	   (cdr (assoc (@:get-config '@pm:drawing-number) content%)))
      (progn 
	(cond
	  ((= (angtos boxRotate 0 0) "0" ); boxRotate 0.3) ;; 横框
	   (setq p2 (list (- (nth 0 boxInsp)
			     (* 100.0  boxScale (float (nth 0 tufu-number))))
			  (+ (nth 1 boxInsp)
			     (* 100.0  boxScale (float (nth 1 tufu-number))))
			  0)) ;p1 根据图幅比例，转角计算
	   (setq zongheng "L"))
	  ((= (angtos boxRotate 0 0) "180" ); boxRotate 0.3) ;; 横框
	   (setq p2 (list (+ (nth 0 boxInsp)
			     (* 100.0  boxScale (float (nth 0 tufu-number))))
			  (- (nth 1 boxInsp)
			     (* 100.0  boxScale (float (nth 1 tufu-number))))
			  0)) ;p1 根据图幅比例，转角计算
	   (setq zongheng "L"))
	  ((= (angtos boxRotate 0 0) "270" ); boxRotate 0.3) ;; 横框
	   (setq p2 (list (+ (nth 0 boxInsp)
			     (* 100.0  boxScale (float (nth 1 tufu-number))))
			  (+ (nth 1 boxInsp)
			     (* 100.0  boxScale (float (nth 0 tufu-number))))
			  0)) ;p1 根据图幅比例，转角计算
	   (setq zongheng "P"))
	  ((= (angtos boxRotate 0 0) "90" ); boxRotate 0.3) ;; 横框
	   (setq p2 (list (- (nth 0 boxInsp)
			     (* 100.0  boxScale (float (nth 1 tufu-number))))
			  (- (nth 1 boxInsp)
			     (* 100.0  boxScale (float (nth 0 tufu-number))))
			  0)) ;p1 根据图幅比例，转角计算
	   (setq zongheng "P"))
	  )
	(setq dir-path (@:mkdir
			(append (@:path (@:get-config '@pm:projects-output))
				(list (cdr (assoc "项目名称" content%))
				      (cdr (assoc "工程名称" content%))
				      (cdr (assoc "子项名称" content%))
				      (@pm:get-tubie content%))
				)))
	(if (= (@:get-config '@pm:print-tuming) 1)
	    (progn
	      (setq tuming (@:string-subst "" " "
					   (strcat (cdr (assoc "图名第一行" content%))
						   (cdr (assoc "图名第二行" content%)))))
	      ;; 替换特殊字符
	      (setq tuming (@:string-subst "／"  "/" tuming))
	      (setq tuming (@:string-subst "："  ":" tuming))
	      (setq tuming (@:string-subst "；"  ";" tuming))
	      (setq tuming (@:string-subst "、"  "\\" tuming))
	      (if (> (strlen tuming) 0)
		  (setq tuming (strcat "_" tuming)))
	      )
	    (setq tuming "")
	    )
	(setq file-name (strcat  dir-path "\\"
				 (if (and
				      (@pm:have-tubie-attribute content%)
				      (@pm:get-tubie content%))
				     (strcat (@pm:get-tubie content%) "-" (cdr (assoc (@:get-config '@pm:drawing-number) content%)))
				     (cdr (assoc (@:get-config '@pm:drawing-number) content%)))
				 tuming
				 ".pdf"))
	
	;;建立工程目录 建设单位 工程名称 子项名称 施工图 专业 
	;; 打印需要的数据 p1 p2 两个点，保存的文件名(含目录)
	(if  (and tufu-number
		  (cdr (assoc (@:get-config '@pm:drawing-number) content%)))
	     (progn
	       (princ (strcat (rtos (nth 1 tufu-number)) "x" (rtos (nth 0 tufu-number))
			      ": "(rtos boxScale)  "\n"))
	       (if (= 1 pdf)
		   (plot:to-pdf tufu zongheng boxInsp p2 file-name))
	       (if (= 0 pdf)
		   (plot:to-dev tufu zongheng boxInsp p2)))
	     (if (cdr (assoc (@:get-config '@pm:drawing-number) content%))
		 (@:log "WARR" (strcat (cdr (assoc (@:get-config '@pm:drawing-number) content%)) tuming " 为非标准图框，不能打印。"))
		 (@:log "WARR" "非标准图框，不能打印。"))
	     
	     ))
      (@:log "WARR" (strcat (if (cdr (assoc (@:get-config '@pm:drawing-number) content%)) (cdr (assoc (@:get-config '@pm:drawing-number) content%))) tufu  " 本程序不支持"))
      )
  )
