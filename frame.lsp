;;; 图框相关函数
(defun @pm:frame-modify-scale (frames / scale)
  "检查图框属性 比例 的值 是否与实际比例一致。"
  (@:help "将图框属性 -比例- 的值 修改为与图框的实际比例一致。")
  (if (= 'ENAME (type frames))
      (setq frames (list frames)))
  (if (= 'pickset (type frames))
      (setq frames (pickset:to-list frames)))

  (foreach frame frames
	   (setq scale (entity:getdxf frame '(41 42 43)))
	   (if (equal (car scale)(cadr scale) 0.001)
	       (block:set-attributes
		frame
		(list (cons "比例"
			    (strcat "1:" (itoa (fix (* 100 (car scale))))))))))
  )

(defun @pm:export-frame ()
  (@::help '("导出当前图形中的图框到标准图库"
	     "后面新建的dwg就可以使用这个图框了"))
  (@:prompt "请选择图框块:")
  (if (setq tk (car (pickset:to-list(ssget ":E:S" '((0 . "insert"))))))
      (progn
	(setq blkname (block:get-effectivename tk))
	(setvar "cmdecho" 0)
	(if (findfile (strcat "D:\\Design\\standard\\"blkname".dwg"))
	    (command "-wblock" (strcat "D:\\Design\\standard\\"blkname".dwg") "y" blkname)
	    (command "-wblock" (strcat "D:\\Design\\standard\\"blkname".dwg") blkname))
	(setvar "cmdecho" 1)
	(@:prompt "成功导出图框定义。")
	))
  (princ)
  )
(defun @pm:set-tukuang ( / blkref tk-name vn0 content% corner)
  "选择一个图框，设置要处理的图框块."
  (@:help (strcat "对图框块的最低要求是，具有属性标记 图号 以及 工程名称 或 项目名称.\n"
		  "    如具有动态块的图幅特性，则会有更好的体验。"))
  (@:prompt "请点选一个图框:")
  (setq blkref (car (pickset:to-list(ssget ":E:S" '((0 . "insert")(66 . 1))))))
  (if (and blkref
	   (setq blkname (block:get-effectivename blkref))) ;; 有名
      (progn
	(setq content% (@pm:get-frame-info blkref))
	;; 识别图号属性标签
	(setq maybe-numbers (string:to-list (@:get-config '@pm:drawing-numbers) ","))
	(setq attdefs (block:get-attdef blkname))
	(while (and (setq attdef% (car attdefs))
		    (not (member (car attdef%) maybe-numbers))
		    (not (member (cadr attdef%) maybe-numbers))
		    )
	  (setq attdefs (cdr attdefs)))
	(if attdefs
	    (setq att-tuhao (caar attdefs))
	    (setq att-tuhao (if (setq res
				      (ui:select "请选择用于图号的属性标签名，如果没有请点取消。"
						 (mapcar 'car 
							 (block:get-attdef blkname))))
				res "")))
	(setq maybe-profession-names '("专业" "图别"))
	
	(setq attdefs (block:get-attdef blkname))
	(while (and (setq attdef% (car attdefs))
		    (not (member (car attdef%) maybe-profession-names))
		    (not (member (cadr attdef%) maybe-profession-names))
		    )
	  (setq attdefs (cdr attdefs)))
	(if attdefs
	    (setq att-profession (caar attdefs))
	    (setq att-profession (if (setq res
					   (ui:select "请选择用于专业或图别的属性标签名，如果没有请点取消。"
						      (mapcar 'car 
							      (block:get-attdef blkname))))
				     res "")))
	;; 识别项目属性标签
	(if (and
	     att-tuhao
	     (or (assoc "项目名称" content%)
		 (assoc "工程名称" content%)
		 (assoc "[工程名称]" content%)
		 (assoc "子项名称" content%)))
	    (progn	
	      (@:set-config  '@pm:tukuang (block:get-effectivename blkref))
	      ;; (@:set-config  '@pm:tukuang-corner (if (assoc "可见性" content%) 0 0))
	      (@:set-config '@pm:drawing-number  att-tuhao)
	      (@:set-config '@pm:profession-name att-profession)
	      (setq boxInsp (cdr (assoc "boxInsp" content%)))
	      (setq box (apply 'point:rec-2pt->4pt (entity:getbox blkref 0)))
	      (if (> (apply 'min (mapcar '(lambda(x)(distance x boxInsp)) box))
		     (* 0.001  (distance (car box)(cadr box))))
		  (@:set-config '@pm:tukuang-corner 1)
		  (@:set-config '@pm:tukuang-corner 0))
	      (@:set-config '@pm:tukuand-base
			    (cond
			      ((< (distance BoxInsp  (car box)) (distance BoxInsp  (cadr box)))
			       1)
			      (t 0)))
	      
	      (@:alert (strcat "当前工程管理的图框块名为 \n"  (block:get-effectivename blkref) " 。")))
	    (@:alert "所选图框不符合本程序运行要示。"))
	(princ))
      (progn ;; 输入图框块名
	(@:alert (strcat "所选块图元不是块，或者为匿名块或无属性块，不满足本程序要求。\n"
			 "请选择图框块文件，这个文件必需是wblock导出的块定义dwg文件。"
			 ))
	(setq tk-name
	      (if (setq tk-file (getfiled "选择图框块文件""D:/Design/standard/" "dwg" 8))
		  (vl-filename-base tk-file)
		  (getstring (strcat "请输入一个图框块名称 <" (@:get-config '@pm:tukuang) ">: "))))
	(if (and (p:stringp tk-name) (/= "" tk-name))
	  (progn
	    (@:set-config '@pm:tukuang tk-name)
      	    (@:alert (strcat "当前工程管理的图框块名设置为 \n"  (@:get-config '@pm:tukuang) " 。"
			     "\n 因为是手动输入的，有可能不满足程序要求。"))
	    (princ))))
      
      ))

(defun @pm:handle-tufu (tufu)
  (setq tufu (string:subst-all "0.25" "1/4" tufu))
  (setq tufu (string:subst-all "0.5" "1/2" tufu))
  (setq tufu (string:subst-all "0.75" "3/4" tufu))
  (setq tufu (string:subst-all "1.25" "5/4" tufu)))
(defun @pm:get-frames-backend (s1 / ss-list ename en0 ti% p ml-contents xmax ymin boxscale insp order)
  "过滤选择集中的图框,返回选中的图元列表"
  (if s1
      (setq ss-list (vl-remove-if-not '(lambda (x)(= (block:get-effectivename x) (@:get-config '@pm:tukuang)))
				      (pickset:to-list s1)))
    (progn
      (@:alert (strcat "未选中任何图框，请检查配置文件中你的图框块名是 "
		       (@:get-config '@pm:tukuang)))
      (@:edit-config))
    )

  (setq order (@:get-config '@pm:drawing-number-order))
  (if (null (wcmatch order  "[xyXY][xyXY]"))
      (setq order "xY"))
  (setq ss-list (pickset:sort ss-list order (* 10 (@:get-config '@pm:tukuang-scale))))
  (if (= 1 (@::get-config '@pm:drawing-number-sub-order))
      (setq ss-list (list:group-by
		     ss-list
		     '(lambda(x y)
			(cond
			 ((string-equal "x" (substr order 1 1))
			  (equal (car (entity:getdxf x 10))
				 (car (entity:getdxf y 10))
				 (* 0.0001 (apply 'distance (entity:getbox x 0)))))
			 ((string-equal "y" (substr order 1 1))
			  (equal (cadr (entity:getdxf x 10))
				 (cadr (entity:getdxf y 10))
				 (* 0.0001 (apply 'distance (entity:getbox x 0)))))
			 )))))
  ss-list
  )

(defun @pm:get-frames (/ s1 )
  "选取图中的图框,返回选中的图元列表"
  (if @:*auto-mode*
      (setq s1 (ssget "x" '((0 . "INSERT"))))
    (progn
      (prompt "回车或右键则为所有图框")
      (setq s1 (ssget '((0 . "INSERT"))))
      (if (null s1)
	  (setq s1 (ssget "x" '((0 . "INSERT")))))))
  (@pm:get-frames-backend s1)
  )

(defun @pm:get-all-frames (/ s1 )
  "选取图中的图框,返回选中的图元列表"
  (setq s1 (ssget "x" '((0 . "INSERT"))))
  (@pm:get-frames-backend s1)
  )
(defun @pm:insert-tukuang-backend (downfile dwg-file pt)
  (if (null (findfile (strcat "packages/" downfile )))
      (progn
	(@:load-module 'pkgman)
	(@:down-pkg-file (@:uri) downfile "stable")(@:alert (strcat "正在下载所需的 " dwg-file ",请稍候。"))(sleep 5)))
  ;; 检测文件夹是否存在
  (if (null (vl-file-directory-p (@:get-config '@pm:tuku)))
      (@:mkdir (@:path (@:get-config '@pm:tuku))))
  ;;(alert (strcat "没有发现图库文件夹，请先创建 " (@:get-config '@pm:tuku)))
  (if (null (findfile (strcat (@:get-config '@pm:tuku) "\\" dwg-file ".dwg")))
      (vl-file-copy (findfile (strcat "packages\\" downfile)) (strcat (@:get-config '@pm:tuku ) "\\" dwg-file ".dwg"))
    )
  ;;;(@:debug "TEST" "insert-tukuang")
  (if (findfile (strcat (@:get-config '@pm:tuku ) "\\" dwg-file ".dwg"))
      (progn 
	(setq tk (block:insert dwg-file (system:dir(@:get-config '@pm:tuku )) pt 0 (@::get-config '@pm:tukuang-scale)))
	(block:set-attributes tk
			      (list (cons "年"  (datetime:get-current-year))
				    (cons "月"  (datetime:get-current-month))
				    (cons "日"  (datetime:get-current-day))
				    (cons "日期" (strcat (datetime:get-current-year) "." (datetime:get-current-month)))
				    )
			      )
	(if (and (vla-get-isdynamicblock (e2o tk));;动态块
		 (@:get-config '@pm:tukuang-mapsheet))
	    (block:set-dynprop tk "map-sheet" (@:get-config '@pm:tukuang-mapsheet)))
	tk
	)))
(defun @pm:insert-tukuang (downfile dwg-file)
  (setq pt '(0 0 0))
  (ui:dyndraw (@pm:insert-tukuang-backend downfile dwg-file pt) pt)
  )
			   

(defun @pm:array-frames (/ dir i% pt pt2 dist-min ang)
  (@:help (strcat "阵列出固定个数的图框。"))
  (setq tk1
	(car 
	 (@pm:insert-tukuang "at-pm/map-sheet.dwg" (@:get-config '@pm:tukuang))))
  (setq pt (entity:getdxf tk1 10))
  (setq box (entity:getbox tk1 0))
  (setq box-h (- (cadr (cadr box))(cadr (car box))))
  (setq box-w (- (car (cadr box))(car (car box))))
	
			  
  (setq pt2 (getpoint pt "请输入第二点，用于确定图块排列方式:"))
  (if (> (setq ang (angle pt pt2)) pi)
      (setq ang (- ang pi)))
  (if (< 0.615456 ang 2.52614)
      (setq dist-min (max (+ (/ box-h (abs (sin ang)))
			     (expt 10 (fix (1- (/ (log box-h) (log 10))))))
			  (distance pt pt2)))
      (setq dist-min (max (+ (/ box-w (abs (cos ang)))
			     (expt 10 (fix (1- (/ (log box-w) (log 10))))))
			  (distance pt pt2))))
  (print ang)(print " ")(print dist-min)
  (setq i% 1)
  (repeat 10
	  (setvar "attreq" 0)
	  (setq tk
		(@pm:insert-tukuang-backend "at-pm/map-sheet.dwg" (@:get-config '@pm:tukuang)
					    (polar pt (angle pt pt2) (* i% dist-min))
					    ))

	  (block:set-attributes tk
				(list (cons "年"  (datetime:get-current-year))
				      (cons "月"  (datetime:get-current-month))
				      (cons "日"  (datetime:get-current-day))
				      (cons "日期" (strcat (datetime:get-current-year) "." (datetime:get-current-month)))

				      )
				
				)
	  (if (and (vla-get-isdynamicblock (e2o tk));;动态块
		   (@:get-config '@pm:tukuang-mapsheet))
	      (block:set-dynprop tk "map-sheet" (@:get-config '@pm:tukuang-mapsheet)))
	  (setq i% (1+ i%)))
  )

(defun @pm:get-layout (content%)
  (cdr (assoc "layout" content%)))

(defun @pm:frames-align (/ A ALL B C L M N P VAL X0 XYZ_NEW Y Y0 Z)
  (@:help 
   "对多个乱序排列的图框自上而下排列。")
  (setq a (@pm:get-frames))
  
  (setq p (getpoint "\n首个图框的插入点："))
  (setq x0 (car p))
  (setq y (cadr p))
  (setq all a)
  (setq m 0)
  (setq n (length a))
  (while (< m n)
    (setq en0 (nth m all))
    (setq b (entget en0))
    (setq z (nth 3 (assoc '10  b)))
    (setq xyz_new (list '10 x0 y z))
    (setq y (- y (cdr (assoc "height" (block:get-dynamic-properties en0)))))  ;; 图框高 ;;(* m val)))
    ;;(setq b (subst (cons '72 0) (assoc '72 b) b))
    ;;(setq b (subst (cons '73 0) (assoc '73 b) b))
    (setq b (subst xyz_new (assoc '10 b) b))
    (entmod b)
    (setq m (1+ m))
    )
  )
(defun @pm:select-tukuang ()
  "选择框选范围内或当前dwg内的所有图框"
  (sssetfirst nil (pickset:from-entlist(list:flatten (@pm:get-frames)))))
(defun @pm:get-corner (content% / tufu boxScale boxRotate BoxInsp tufu-number p2 )
  ;;(setq content% (nth ti% s1))
  (setq tufu (@pm:get-map-sheet content%))
  (setq boxScale (float (cdr (assoc "boxScale" content%))))
  (setq boxRotate (float (cdr (assoc "boxRotate" content%))))
  
  (setq boxInsp (cdr (assoc "boxInsp" content%)))
  (if (and (/= tufu "非标准框")(= 0 (@:get-config '@pm:tukuang-corner))
	   (setq tufu-number (cdr (assoc tufu  @:paper-size)))
	   (cdr (assoc (@:get-config '@pm:drawing-number) content%)))
      (progn 
	(cond
	 ((= (angtos boxRotate 0 0) "0" ); boxRotate 0.3) ;; 横框
	  (setq p2 (list (- (nth 0 boxInsp)
			    (* 100.0  boxScale (float (nth 0 tufu-number))))
			 (+ (nth 1 boxInsp)
			    (* 100.0  boxScale (float (nth 1 tufu-number))))
			 0)) ;p1 根据图幅比例，转角计算
	  (setq zongheng "L"))
	 ((= (angtos boxRotate 0 0) "180" ); boxRotate 0.3) ;; 横框
	  (setq p2 (list (+ (nth 0 boxInsp)
			    (* 100.0  boxScale (float (nth 0 tufu-number))))
			 (- (nth 1 boxInsp)
			    (* 100.0  boxScale (float (nth 1 tufu-number))))
			 0)) ;p1 根据图幅比例，转角计算
	  (setq zongheng "L"))
	 ((= (angtos boxRotate 0 0) "270" ); boxRotate 0.3) ;; 横框
	  (setq p2 (list (+ (nth 0 boxInsp)
			    (* 100.0  boxScale (float (nth 1 tufu-number))))
			 (+ (nth 1 boxInsp)
			    (* 100.0  boxScale (float (nth 0 tufu-number))))
			 0)) ;p1 根据图幅比例，转角计算
	  (setq zongheng "P"))
	 ((= (angtos boxRotate 0 0) "90" ); boxRotate 0.3) ;; 横框
	  (setq p2 (list (- (nth 0 boxInsp)
			    (* 100.0  boxScale (float (nth 1 tufu-number))))
			 (- (nth 1 boxInsp)
			    (* 100.0  boxScale (float (nth 0 tufu-number))))
			 0)) ;p1 根据图幅比例，转角计算
	  (setq zongheng "P"))
	 (tufu-number
	  (setq p2 (polar boxInsp
			  (+ boxRotate (if (= 0 (@:get-config '@pm:tukuang-base))
					   (+ pi (- (atan (/ (float (nth 1 tufu-number))(float (nth 0 tufu-number))))))
					 (atan (/ (float (nth 1 tufu-number))(float (nth 0 tufu-number))))))
			  (sqrt
			   (+
			    (expt (* 100.0  boxScale (float (nth 1 tufu-number))) 2)
			    (expt (* 100.0  boxScale (float (nth 0 tufu-number))) 2))))))
	 )
	(cons (geometry:wcs2ucs boxInsp)  (list (geometry:wcs2ucs p2)))
	)
      (progn
	(setq blkref(cdr (assoc "ename" content%)))
	(mapcar
	 '(lambda(x)
	   (block:bcs2wcs x
	    (vla:get-value(vla-get-origin (block:get-obj-by-name (entity:getdxf (cdr (assoc "ename" content%))2))))
	    boxInsp
	    boxRotate
	    boxScale
	    ))
	 (pickset:getbox
	  (vl-remove-if-not '(lambda(x) (and (= (vla-get-visible (e2o x)):vlax-true)
					 (wcmatch (entity:getdxf x 0)"LINE,LWPOLYLINE")
					 ))
			    (block:ent-list(entity:getdxf (cdr (assoc "ename" content%))2)))
	  0)))
      ))
(defun @pm:replace-oldframe ()
  (@:help '("替换图框,将其它图框替换为当前图框"))
  (@:prompt "请框选要替换的图框,")
  (setq tks (pickset:to-list  (ssget '((0 . "insert")(66 . 1)))))
  ;; 去当前图框
  (setq tks (vl-remove-if
	     '(lambda(x)
	       (equal(block:get-effectivename x)
		(@::get-config '@pm:tukuang)))
	     tks))
  (setq maybe-names (string:to-list(@::get-config '@pm:drawing-names)","))
  (setq tks (vl-remove-if-not
	     '(lambda(x)
	       (apply 'or
		(mapcar '(lambda(title)
			  (assoc title (block:get-attributes x)))
		 maybe-names
		 )))
	     tks))
  (foreach
   tk tks
   (setq box (entity:getbox tk 0))
   (if (= 0 (@::get-config  '@pm:tukuang-base))
       (setq pt-ins (cadr (apply 'point:rec-2pt->4pt box)))
       (setq pt-ins (car (apply 'point:rec-2pt->4pt box))))
   (setq height (- (cadr (cadr box))
		   (cadr (car box))))
   (setq width (- (car (cadr box))
		  (car (car box))))
   
   (setq new-tk
	 (block:insert
	  (@::get-config '@pm:tukuang)
	  "D:\\Design\\standard\\"
	  pt-ins
	  (entity:getdxf tk 50)
	  (entity:getdxf tk 41)))
   ;; (block:set-dynprop new-tk "map-sheet" (entity:getdxf tk 2))
   ;; 替换图名值，并复制其它属性值
   (setq old-attrs  (block:get-attributes tk))
   (setq i 0)
   (while (null (assoc (nth i maybe-names) old-attrs))
     (setq i (1+ i)))
   (if (nth i maybe-names)
       (setq drawing-name (cdr (assoc (nth i maybe-names) (block:get-attributes tk)))))
   (if (null drawing-name)(setq drawing-name ""))
   (block:set-attributes new-tk
			 (if (assoc (@::get-config '@pm:drawing-name) old-attrs)
			     old-attrs
			     (append old-attrs
				     (list (cons (@::get-config '@pm:drawing-name)
						 drawing-name)))))

   ;;如果当前图框为动态块，设置图幅
   (cond
     ((and (equal (vla-get-isdynamicblock(e2o new-tk)) :vlax-true)
	   (equal (vla-get-isdynamicblock(e2o tk)) :vlax-false))
      (progn
	(block:set-dynprop new-tk "map-sheet" (@pm:calc-mapsheet height width)))
      )
     ((and(equal (vla-get-isdynamicblock(e2o new-tk)) :vlax-true)
	  (equal (vla-get-isdynamicblock(e2o tk)) :vlax-true))
      (progn
	(foreach props (block:get-properties tk)
		 (if (not (equal (car props) "Origin"))
		     (block:set-dynprop new-tk (car props)(cdr props)))))
      ))
   (entdel tk)
   ))
(defun @pm:frame-p (blkname / ents box height width mapsheet)
  "测试一个块定义是否为图框，即块内多段线的四角点形成的矩形，
是否符合图幅规定,如果是则返回图幅名及4角点和基点，否则返回nil"
  (if (and (p:enamep blkname)
	   (wcmatch (entity:getdxf blkname 0) "INSERT,BLOCK"))
      (setq blkname (entity:getdxf blkname 2)))
  (if (and (p:stringp blkname)
	   (setq ents (vl-remove-if-not
		       '(lambda(x)
			 (and (wcmatch (entity:getdxf x 0) "LINE,LWPOLYLINE")
			  ;; (= 4 (entity:getdxf x 90))
			  (= (vla-get-visible (e2o x)):vlax-true)
			  ))
		       (block:ent-list blkname)))
	   )
      (progn
	(setq box (pickset:getbox ents 0))
	(setq height (- (cadr (cadr box))
			(cadr (car box))))
	(setq width (- (car (cadr box))
		       (car (car box))))
	(if (setq mapsheet (@pm:calc-mapsheet height width))
	    (list mapsheet
		  (apply 'point:rec-2pt->4pt box)
		  (entity:getdxf (tblobjname "block" blkname) 10)
		  ))
	)))
