(defun @pm:auto (/ frames auto )
  ;; (defun *error* (msg)
  ;;   (princ msg)
    
  ;;   )
  (if (setq frames (@pm:get-frames))
      (progn
	(setq auto
	      (ui:input
	       "请选择要批量操作的项目"
	       (list '("自动编图号" nil "从1开始按自左向右，自上而下的顺序编图号。" )
		     '("修正比例值" T "按图框实际比例修正图框上的比例值。")
		     '("附着签名" T "向图框附着手写签名。")
		     '("生成目录" T "生成 csv 文件格式的目录，用于汇总。")
		     '("打印预处理" T "处理图线，填充的绘制前后次序。")
		     '("批量转PDF" T "将当前dwg中的图框输出成 PDF格式。")
		     '("合并PDF" T "将本次输出的PDF合并成一个文件。")
		     )
	       ))
	(if (> (length frames) 0)
	    (progn
	      (setq @:*auto-mode* T)
	      (if (cdr (assoc "自动编图号" auto))
		  (@pm:set-tuhao frames)) ;; 自动编图号
	      (if (cdr (assoc "修正比例值" auto))
		  (@pm:frame-modify-scale frames))
	      (if (cdr (assoc "附着签名" auto))
		  (@pm:attach-sign frames))
	      (if (cdr (assoc "生成目录" auto))
		  (@pm:make-mulu-csv frames))
	      (if (cdr (assoc "打印预处理" auto))
		  (@pm:pre_plot))
	      (if (cdr (assoc "批量转PDF" auto))
		  (@pm:mprint-pdf frames))
	      (setq @:*auto-mode* nil)
	      )))))
