(defun @pm:open-yunpath (/ contents paths path%)
  "获取云盘的工程目录，"
  ;; 从当前dwg中获取工程名称。
  ;; 如果为多个，按统计图框的工程名称数量排序弹窗选择，
  ;; 如果为空，从云盘中选出最近的工程文件夹，弹窗选择。
  ;; 然后打开云盘工程目录
  (setq contents (@pm:make-all-contents))
  (setq paths (stat:stat(mapcar '(lambda (x)(cdr(assoc "工程名称" x))) contents)))
  (print paths)
  (cond
    ((and (= 1 (length paths));;dwg 唯一
	  (/= "" (caar paths)))
     (system:explorer (strcat (system:dir(@:get-config '@pm:projects-yunpath)) (caar paths))))
    
    ((< 1 (length paths));;dwg 不唯一
     (setq path%
	   (ui:select "请选择工程名称:" (mapcar '(lambda (x)(car x)) paths)))
     (print path%)
     (system:explorer (strcat (system:dir(@:get-config '@pm:projects-yunpath)) path%)))
    ((or (and (= 1 (length paths)) (= "" (caar paths)))
	 (null paths));; 唯一且 空
     ;; 打开云盘最新的目录， vlfile-systime 不能读取目录的时间。
     (alert "打开云盘最新的目录,开发中")
     (setq paths (vl-directory-files (@:get-config '@pm:projects-yunpath) nil -1))
     (setq paths (vl-remove "." paths))
     (setq paths (vl-remove ".." paths))
     (if (< (length paths) 15)
	 (progn
	   (setq path%
		 (ui:select "请选择工程名称:" (reverse paths)))
	   (print path%)
	   (system:explorer (strcat (system:dir(@:get-config '@pm:projects-yunpath)) path%)))
	 (system:explorer (strcat (system:dir(@:get-config '@pm:projects-yunpath))))))
     ;;(setq paths (vl-sort paths
			  ;; '(lambda (x y)
			  ;;   (>
			  ;;    (datetime:mktime
			  ;;     (vl-file-systime
			  ;;      (strcat (system:dir(@:get-config '@pm:projects-yunpath))
			  ;; 	       x)))
			  ;;    (datetime:mktime
			  ;;     (vl-file-systime
			  ;;      (strcat (system:dir(@:get-config '@pm:projects-yunpath))
     ;; 	       y)))))))
    ))
(defun @pm:upload-curr()
  (@:help "发布当前文件到云盘，云盘以当前文件名+日期 命名。")
  (princ)
  )
