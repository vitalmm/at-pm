(@:define-config '@pm:qr-size 1000 "生成的 QR-code 的尺寸与原图像素比")
(defun @pm:make-qr ()
  (@pm:get-qr (entity:getdxf (car (entsel "请选择一个用于生成 QR 的单行文本:")) 1))
  )

(defun @pm:get-qr( str-qr / passwd)
  "获取 qr"
  (if (null (@:check-uploader)) (progn (@:down-uploader)(exit)))
  (sleep 1)
  (@:log "INFO" (strcat "get qr from string ... "))
  ;;(check-uploader)
  (if (null (@:check-userinfo))
      (progn
	(alert (_"User infomation is invalid."))
	(exit)))
  (if (or (/= 'str (type str-qr))
	  (= "" str-qr))
      (setq str-qr "http://atlisp.cn"))
  (setq timestamp  (rtos (* 86400 (getvar "date"))))
  (setvar "cmdecho" 0)
  (command "start-bg"
	   (strcat " /D " @:*prefix*
		   " /MIN /B  "
		   @:*prefix* @:*curl*
		   " -A \"Atlisp-User\" "
		   "   -F \"timestamp=" timestamp
		   "\" -F \"str-qr=#" (vl-princ-to-string (vl-string->list str-qr))
		   "\" -F \"locale=" (getvar "locale")
		   "\" -D " @:*tmp-path* "header-" timestamp 
		   "  -o  " @:*tmp-path* "response-" timestamp 
		   " \"http://atlisp.cn/make-qr\""  ))
  ;; 对response-timestamp 分析
  (setvar "cmdecho" 1)
  (sleep 1)
  (if (findfile (strcat @:*tmp-path* "response-" timestamp))
      (progn
	(setq header (@:parse-header (strcat @:*tmp-path* "header-" timestamp)))
	(if (= 200 (atoi (@:header-code header)))
	    (progn
	      ;; insert img into dwg
	      (setq response (read (@:get-file-contents  (strcat @:*tmp-path* "response-" timestamp))))
	      (if (= T (cdr (assoc ':result response)))
		  (progn
		    (setvar "cmdecho" 0)
		    (@:downfile (cdr (assoc ':qr-url response))
				(strcat (getvar "dwgprefix") "qr-" timestamp ".png")
				timestamp)
		    (setvar "cmdecho" 1)
		    (sleep 1)
		    (princ (strcat (cdr (assoc ':msg response)) "\n"))
		    (if (findfile (strcat (getvar "dwgprefix") "qr-" timestamp ".png"))
			(progn
			  (setvar "filedia" 0)
			  (command "-attach"  (strcat (getvar "dwgprefix") "qr-" timestamp ".png")
				   (getpoint "请输入 QR 插入点: ") (@:get-config '@pm:qr-size) 0)))))
	      )
	    (progn
	      (alert (strcat (_"get qr-code error.")))
	      ))
	))
  )
