(@::def-pkg '((:NAME . "at-pm")
        (:FULL-NAME . "@lisp工程管理")
        (:AUTHOR . "VitalGG")
        (:EMAIL . "vitalgg@gmail.com")
        (:VERSION . "1.5.27")
        (:DESCRIPTION . "@lisp工程管理:基于自定义图框的工程管理、生成图纸目录、批量签名、批量打印、业务逻辑管理等。")
        (:CATEGORY . "图档管理")
        (:URL . "http://atlisp.cn")
        (:OPEN-SOURCE . 0)
        (:REQUIRED . "base")
        (:FILES "at-pm"
            "contents"
            "sign"
            "plot"
            "saveas"
            "outline"
            "frame"
            "maptitle"
            "qr"
            "menus"
            "yun"
            "auto"
            "add-frame-by-ents"
            "beihua"
            "replace-xtsjy"
            "map-sheet.dwg")))
