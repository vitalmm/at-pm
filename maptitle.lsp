;;; 图名智能识别

(defun @pm:get-text (box / strs )
  ;; 获取给定的 box 中的所有文字内容
  (setq strs (pickset:to-list (ssget "w" (car box)(last box) '((0 . "*TEXT,*DRAWINGNAME")))))
  (setq strs
	(mapcar
	 '(lambda(x)
	   (string:subst-all "" " "
	    (entity:getdxf x 1))
	   )
	 strs)))

(defun @pm:menu-pickout-maptitle-readme (/ readme)
  (setq readme (strcat ;; " 调用方法:  (@pm:pickout-maptitle-first box) \n"
		       ;; "    参数 box 为两个点坐标组成的表。形如 ((0.0 0.0 0.0) (100.0 100.0 100.0))\n"
		       ;; "    返回概率最大的那个字符串，当没有选中时，返回 nil.\n\n"
		       "    当识别结果不符合您的预期时，需进行训练。\n"
		       "    进行正向训练时，选择图名字符串。\n"
		       "    进行反向训练时，选择非图名字符串。\n"
		       "    训练时尽量选择特征明显的字符串。\n"
		       ))
  (@::alert readme)
  (princ)
  )
(defun @pm:menu-pickout-maptitle (/ pt1 res)
  (@:help "框选图中文字，根据规则计算出最符合图名的字串。")
  ;;(@:load-module 'at-pm/maptitle)
  (if (setq res
	    (@pm:pickout-maptitle (list (setq pt1 (getpoint "请输入第一点:")) (getcorner pt1))))
      (ui:select-multi "选择识别结果" (mapcar '(lambda (x)(strcat (car x) " (概率: "(rtos (cdr x) 2 3) ")"))
  					      (vl-remove nil(list:get-front-nth 10 res))))
      (progn
	(@::prompt"未识别出图名文字，请进行正向样本及反向样本进行训练。")
	(@pm:menu-pickout-maptitle-readme)
	))
  res)

(defun @pm:pickout-maptitle (box)
  (@:load-module 'at-pm/maptitle)
  (mapcar '(lambda(x) (cons x (@pm:score x)))
	  (vl-remove-if
	   '(lambda (x) (< (@pm:score x) 0.51))
	   (vl-sort (@pm:cut (@pm:get-text box))
		    '(lambda (x y)(> (@pm:score x)(@pm:score y)))))))
(defun @pm:pickout-maptitle-first (box / res)
  (setq res (@pm:pickout-maptitle box))
  (if (>  (cdr (car res))(cdr (cadr res)))
      (car res)
      (list (car res)(cadr res)))
  )

(defun @pm:train-maptitle (/ strs fp)
  (prompt "请选择图名文字")
  (@:load-module 'at-pm/maptitle)
  (setq strs (mapcar '(lambda(x)(entity:getdxf x 1)) (pickset:to-list (ssget '((0 . "*TEXT,*DRAWINGNAME"))))))
  (setq fp  (open (strcat @:*prefix-config* "maptitle+.data") "a"))
  (write-line (string:from-lst strs "") fp)
  (close fp)
  (@pm:load-maptitle-data)
  )
(defun @pm:train-notmaptitle (/ strs fp)
  (prompt "请选择非图名文字")
  (@:load-module 'at-pm/maptitle)

  (setq strs (mapcar '(lambda(x)(entity:getdxf x 1)) (pickset:to-list (ssget '((0 . "*TEXT,*DRAWINGNAME"))))))
  (setq fp  (open (strcat @:*prefix-config* "maptitle-.data") "a"))
  (write-line (string:from-lst strs "") fp)
  (close fp)
  (@pm:load-maptitle-data)
  )
(defun @pm:add-to-whitelist()
  (@::prompt "请选择要设为图名白名单的文字")
  (@:load-module 'at-pm/maptitle)
  (setq strs (mapcar '(lambda(x)(text:remove-fmt(entity:getdxf x 1))) (pickset:to-list (ssget '((0 . "*TEXT,*DRAWINGNAME"))))))
  (setq fp  (open (strcat @:*prefix-config* "maptitle-whitelist.data") "a"))
  (write-line (string:from-lst strs "\n") fp)
  (close fp)
  (@pm:load-maptitle-data))
(defun @pm:add-to-blacklist()
  (@::prompt "请选择要设为图名黑名单的文字")
  (@:load-module 'at-pm/maptitle)
  (setq strs (mapcar '(lambda(x)(text:remove-fmt(entity:getdxf x 1))) (pickset:to-list (ssget '((0 . "*TEXT,*DRAWINGNAME"))))))
  (setq fp  (open (strcat @:*prefix-config* "maptitle-blacklist.data") "a"))
  (write-line (string:from-lst strs "\n") fp)
  (close fp)
  (@pm:load-maptitle-data))
(defun @pm:save-drawingno-data (/ fp)
  (setq fp (open  (strcat @:*prefix-config* "maptitle+.data") "w"))
  (write-line (l2s-ansi (vl-sort (list:remove-duplicates @pm:drawingno+) '>)) fp)
  (close fp)
  (setq fp (open  (strcat @:*prefix-config* "maptitle-.data") "w"))
  (write-line (l2s-ansi (vl-sort (list:remove-duplicates @pm:drawingno-) '>)) fp)
  (close fp))

;;(@:add-menu "图名图号" "图号说明" "(@pm:menu-pickout-drawingno-readme)")
(defun @pm:menu-pickout-drawingno-readme (/ readme)
  (setq readme (strcat " 调用方法:  (@pm:pickout-drawingno-first box) \n"
		       "    参数 box 为两个点坐标组成的表。形如 ((0.0 0.0 0.0) (100.0 100.0 100.0))\n"
		       "    返回概率最大的那个字符串，当没有选中时，返回 nil."))
  (@:help readme)
  (alert readme)
  (princ)
  )
;;(@:add-menu "图名图号" "识别图号" "(@pm:menu-pickout-drawingno)")
(defun @pm:menu-pickout-drawingno (/ pt1 res)
  (@:help "框选图中文字，根据规则计算出最符合图号的字串。")
  (setq res
	(@pm:pickout-drawingno (list (setq pt1 (getpoint "请输入第一点:"))
			      (getcorner pt1))))
  (ui:select-multi "选择识别结果" (mapcar '(lambda (x)(strcat (car x) " (概率: "(rtos (cdr x) 2 3) ")"))
				   res)))
(defun @pm:pickout-drawingno (box)
  (mapcar '(lambda(x) (cons x (@pm:score x)))
	  (vl-remove-if
	   '(lambda (x) (< (@pm:score x) 0.51))
	   (vl-sort (@pm:cut-drawingno (@pm:get-text box))
		    '(lambda (x y)(> (@pm:score x)(@pm:score y)))))))
(defun @pm:pickout-drawingno-first (box)
  (car (@pm:pickout-drawingno box)))

;;(@:add-menu "图名图号" "图号正向训练" "(@pm:train-drawingno)")
(defun @pm:train-drawingno (/ strs)
  (@pm:load-drawingno-data)
  (prompt "请选择图号文字")
  (setq strs (mapcar '(lambda(x)(entity:getdxf x 1)) (pickset:to-list (ssget '((0 . "*TEXT,*DRAWINGNAME"))))))
  (setq @pm:drawingno+ (append @pm:drawingno+ (vl-remove 32 (s2l-ansi (apply 'strcat strs)))))
  (@pm:save-drawingno-data)
  )
;;(@:add-menu "图名图号" "图号反向训练" "(@pm:train-notdrawingno)")
(defun @pm:train-notdrawingno (/ strs)
  (@pm:load-drawingno-data)
  (prompt "请选择不是图名的文字")
  (setq strs (mapcar '(lambda(x)(entity:getdxf x 1)) (pickset:to-list (ssget '((0 . "*TEXT,*DRAWINGNAME"))))))
  (setq @pm:drawingno- (append @pm:drawingno- (vl-remove 32 (s2l-ansi (apply 'strcat strs)))))
  (@pm:save-drawingno-data)
  )

(defun @pm:clean-example ()
  (@::help '("清空识别样本"))
  (if (= "@LISP"(getstring (@::prompt"请输入大写的 @LISP ,以确定你要清空样本:")))
      (progn
	(foreach
	 file% '("maptitle+.data"
		 "maptitle-.data"
		 "maptitle-whitelist.data"
		 "maptitle-blacklist.data")
	 (if (findfile (strcat @::*prefix-config* file%))
	     (vl-file-delete
	      (findfile (strcat @::*prefix-config* file%)))))
	(@::prompt"已成功清理。")
	(if @pm:load-maptitle-data
	    (@pm:load-maptitle-data)
	    ))
      (@::prompt"中断清理。")
      )
  (princ)
  )
;; 图名相关
(defun @pm:setup-maptitle ()
  (@::help '("选择图名文字，然后写入选定的图框的图名中。"))
  (@::prompt "请选择一个或多个图名文字，选完回车或鼠标右键:")
  (if (setq titles (pickset:to-list(ssget '((0 . "*text,TCH_DRAWINGNAME")))))
      (progn
	(setq strs
	      (mapcar
	       '(lambda(x)
		 (string:subst-all "" " "
		  (text:remove-fmt (entity:getdxf x 1))))
	       titles))
	(@::prompt "请选择要写入图名的图框")
	(if (setq tk (car(pickset:to-list(ssget ":E:S"  '((0 . "insert")(66 . 1))))))
	    (block:set-attributes tk
				  (list
				   (cons (@:get-config '@pm:drawing-name)
					 (string:from-list strs "\\P")
					 ))))))
  )
(defun @pm:Intelligent-setup-maptitle ( / en0 ss1 str-name maptitle corner)
  (@:help (strcat "选择一组图框，把图框范围内最像图名的文字设为图框的图名。需先进行训练。"
		  "\n使用演示说明:\n http://atlisp.cn/stable/at-pm/Map-name-recognition.mp4\n"))
  ;;"选择图名列表模板。生成一系列图名"
  (if (@:internetp)
      (progn
	(prompt "请选择一(个)组图框:")
	
	(foreach content% (@pm:make-contents)
		 ;;(setvar "ctab" layout)
		 (setq corner (@pm:get-corner content%))
		 (command "zoom" "w" (car corner) (cadr corner))
		 (if (setq maptitles (@pm:pickout-maptitle (@pm:get-corner content%)))
		     (progn
		       (if (equal (cdr (car maptitles))(cdr (cadr maptitles)) 0.0001)
			   (setq maptitle (strcat (car(car maptitles))
						  "\\P"
						  (car(cadr maptitles))))
			 (setq maptitle (caar maptitles))
			 )
		       (if (null (member (@:get-config '@pm:drawing-name) (mapcar 'car (block:get-attributes (cdr (assoc "ename" content%))))))
			   (progn
			     (@:alert "图名属性标签不正确，请正确设置")
			     (@:set-config '@pm:drawing-name (if (setq res (ui:select "请选择正确的图名标签" (mapcar 'car (block:get-attributes (cdr (assoc "ename" content%))))))
								 res ""
								 ))))
		       (if maptitle
			   (block:set-attributes (cdr (assoc "ename" content%))   (list (cons (@:get-config '@pm:drawing-name) maptitle))))
		       )))
	))
  )
