(@:define-config '@:outline-curr-page 0 (_"outline current page"))
(@:define-config '@:outline-per-page 20 (_"outline number of one page"))

(defun @pm:outline-dialog (/ dcl_fp dcl-tmp dcl_id pkg para% menu%
			   frames curr-page per-page after-panel-cmd
			   zoom-w
			   run-function after-panel corner
			   page-up page-down *error* )
  "图纸大纲，用于快速切换图纸视图"
  (@:help "图纸大纲，用于快速切换图纸视图\n   快捷键 vv ")
  (defun *error* (msg)
    ;; 重启动处理 
    (if (= 'file (type dcl_fp))
	(close dcl_fp))
    (unload_dialog dcl_id)
    (vl-file-delete dcl-tmp)
    (@:*error* msg)
    )
  ;;(setq frames (@pm:make-all-contents))
  (setq frames (@pm:frames2contents (list:flatten(@pm:get-all-frames))))
  (setq per-page (@:get-config '@:outline-per-page))
  (if (<= (length frames) per-page)
      (setq curr-page 0)
      (setq curr-page (@:get-config '@:outline-curr-page)))
  (defun zoom-w (corner layout)
    ;; change layout
    (print layout)
    (setvar "ctab" layout)
    (command "zoom" "w" (car corner) (cadr corner)))
  (defun after-panel ( func )
    (if (= 'str (type func))
	(eval (read func))))
  (defun page-up ()
    (setq curr-page (1- curr-page))
    (@:set-config '@:outline-curr-page curr-page)
    (done_dialog 10)
    (setq after-panel-cmd "(@pm:outline-dialog)"))
  (defun page-down ()
    (setq curr-page (1+ curr-page))
    (@:set-config '@:outline-curr-page curr-page)
    (done_dialog 10)
    (setq after-panel-cmd "(@pm:outline-dialog)"))
  (defun run-function (corner)
    ;; (princ func)
    (done_dialog 10)
    (setq after-panel-cmd corner))
  (if frames
      (progn
	;; 生成 dcl 文件
	(setq dcl-tmp (strcat @:*tmp-path* "tmp-outline.dcl" ))
	(setq dcl_fp (open dcl-tmp "w"))
	(write-line (strcat "panel : dialog {"
			    "label = \"图纸切换\"; ")
		    dcl_fp)
	(setq i% 0)(setq bt-width 38)
	(write-line ":image{ height=0.1; color=250; fixed_height=true;}:row{label=\"\";" dcl_fp)
	(setq c% 0)(setq j% 0)
	;;(setq bt-menu-column (nth (+ c% (* per-page curr-page)) menus-list))
	;; 一列数据
	(foreach tk%  frames
		 (if (= 0 (rem j% per-page))
		     (progn
		       (setq flag-col T)
			 (write-line (strcat ":column{label=\"\";children_alignment=top;fixed_width=true;children_fixed_width=true;width="
					     (itoa bt-width) ";" )
				     dcl_fp)))
		   (setq r% 0)
		   (progn
		     (write-line (strcat ":button{ fixed_width=true;children_fixed_width=true;children_alignment=left;width="
					 (itoa bt-width)
					 ";fixed_height=true;"
					 " key=\"c""_"(itoa (setq j% (1+ j%)))"\"; "
					 "label=\""
					 (strcat (if (and
						      (@pm:have-tubie-attribute tk%)
						      (setq tubie (@pm:get-tubie tk%)))
						     (strcat tubie"-")"")
						 (cdr (assoc (@:get-config '@pm:drawing-number) tk%)) ":" (@pm:get-tuming tk%)) "\"; "
					 " action=\"(run-function \\\"(zoom-w '"
					 (@:string-subst "\\\\\\\"" "\"" (vl-prin1-to-string (@pm:get-corner tk%)))
					 " " "\\\\\\\""
					 (@:string-subst "\\\\\\\"" "\"" (@pm:get-layout tk%))"\\\\\\\""
					 ")\\\")\";is_enabled=true;}")
				 dcl_fp)
		     (setq r% (1+ r%)))
		   ;;(write-line "}" dcl_fp)
		   (setq c% (1+ c%))(setq i% (1+ i%))
		   (if (or (and flag-col  (= 0 (rem j% per-page)))
			   (= j% (length frames)))
		       (progn 
			 (write-line "}" dcl_fp)
			 (setq flag-col nil)))
		   )
	(write-line "}" dcl_fp)
	(write-line ":image{ height=0.1; color=250; fixed_height=true;}" dcl_fp)
	;;分页
	;; (if (> (length frames) per-page)
	;;      (write-line ":row{alignment=centered;children_alignment=centered;:button{label=\"<\";key=\"prev\";is_enabled=false;}:spacer{} :text_part{key=\"curr_total\"; value=\"\";alignment=\"centered\";width=10;}:button{label=\">\";key=\"next\";is_enabled=false;}}"
	;;  		dcl_fp))
	(write-line " :spacer {} ok_cancel; }" dcl_fp)
	(close dcl_fp)
	
	(setq dcl_id (load_dialog dcl-tmp))
	(if (not (new_dialog "panel" dcl_id))
	    (exit))
	
	(action_tile "accept" "(done_dialog 1)")
	(action_tile "prev" "(page-up)")
	(action_tile "next" "(page-down)")
	(if (= 0 curr-page) (mode_tile "prev" 1) (mode_tile "prev" 0))
	(if (= (/ (1- (length frames)) per-page) curr-page) (mode_tile "next" 1) (mode_tile "next" 0))
	(start_dialog)
	(unload_dialog dcl_id)
	(vl-file-delete dcl-tmp)
	(after-panel after-panel-cmd))
      ))
;;(@:define-hotkey "vv" "(@pm:outline-dialog)")
