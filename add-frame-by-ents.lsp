(defun @pm:calc-mapsheet (height width / i paper ps)
  ;; 考虑绘图比例
  (setq i -1)
  (while (and (< (setq i (1+ i))(length @:paper-size))
	      (or 
	       (>= height(* (@:get-config '@::draw-scale) (+ (caddr (nth i @:paper-size)) 2)))
	       (>= width (* (@:get-config '@::draw-scale)(+ (cadr (nth i @:paper-size)) 2))))))
  (if (< i (length @:paper-size))
      (setq paper  (car (nth i @:paper-size)))
    ;; 按合适的长宽比例缩放图纸
    (progn
      (setq ps (vl-sort @:paper-size '(lambda (x y)
					(or (and (equal (/ (cadr x)(caddr x)) (/ (cadr y)(caddr y)) 0.001)
						 (> (caddr x)(caddr y)))
					    (< (/ (cadr x)(caddr x))(/ (cadr y)(caddr y)))))))
      (setq i -1)
      (while (< (/ width height) (/ (cadr (nth (setq i (1+ i)) ps)) (caddr (nth i ps)))))
      (if (< i (length @:paper-size))
	  (setq paper (car (nth i ps)))
	(setq paper (car (last ps)))
	)
      ))
  paper)
(defun @pm:add-frame-by-box (box / height width paper tk)
  (if(< (apply 'angle box) (* 0.25 pi))
      (setq box
	    (list
	     (polar 
	      (polar (car box) pi (@:scale 25))
	      (* 1.5 pi) (@:scale 10))
	     (polar 
	      (polar (cadr box) 0 (@:scale 80))
	      (* 0.5 pi) (@:scale 10))))
    (setq box
	  (list
	   (polar 
	    (polar (car box) pi (@:scale 10))
	    (* 1.5 pi) (@:scale 80))
	   (polar 
	    (polar (cadr box) 0 (@:scale 10))
	    (* 0.5 pi) (@:scale 25)))))

  
  (setq height (- (cadr (cadr box))
		  (cadr (car box))))
  (setq width (- (car (cadr box))
		 (car (car box))))
  
  (if (> height (@:scale 100))
      (if (< (/ height width) 1.0)
	  (progn 
	    (setq paper (@pm:calc-mapsheet height  width))
	    ;;计算插入点
	    (setq tk
		  (block:insert
		   (@::get-config '@pm:tukuang)
		   (strcat (@::get-config '@pm:tuku)"\\")
		   (polar 
		    (polar (point:centroid box) (* 1.5 pi)
			   (* 0.5 (@::get-config '@::draw-scale)
			      (cadr (cdr (assoc paper @:paper-size)))))
		    0
		    (* 0.5 (@::get-config '@::draw-scale)
		       (car (cdr (assoc paper @:paper-size)))))
		   0 (@:scale 0.01)))
	    (block:set-dynprop tk "map-sheet" paper)
	    )
	(progn 
	  (setq paper (@pm:calc-mapsheet width height))
	  ;;计算插入点
	  (setq tk
		(block:insert
		 (@::get-config '@pm:tukuang)
		 (strcat (@::get-config '@pm:tuku)"\\")
		 (polar 
		  (polar (point:centroid box) (* 1.0 pi)
			 (* 0.5 (@::get-config '@::draw-scale)
			    (cadr (cdr (assoc paper @:paper-size)))))
		  (* 1.5 pi)
		  (* 0.5 (@::get-config '@::draw-scale)
		     (car (cdr (assoc paper @:paper-size)))))
		 (* 1.5 pi) 1))
	  (block:set-dynprop tk "map-sheet" paper)
	  )

	)
    (@::prompt "图形高度太小，跳过套图框")
    ))

(defun @pm:add-frame-by-ents ()
  (@::help '("选择图形并绘制图框"))
  (@:prompt "请选择要加图框的图形:")
  (setq ss (ssget))
  (setq box (pickset:getbox ss 0))
  ;; 计算外框
  (@pm:add-frame-by-box box)
  )

(defun @pm:add-frame-by-cluster ()
  (foreach box (pickset:cluster (ssget) (@:scale 100))
	   (@pm:add-frame-by-box(pickset:getbox (ssget "w"(car  box)(cadr box)) 0)))
  )
