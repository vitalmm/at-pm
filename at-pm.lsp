(defun @pm:setup (/ res)
  "工程管理基本信息"
  (setq @::tmp-search-str "@pm")
  (@::edit-config-dialog)
  )


;;(@:down-pkg-file (@:uri) "base/lib-ui.lsp" "stable")
;;; 配置参数
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(@:define-config '@pm:user-name "张三" "你的姓名，用于对比图框设计人进行过滤。只打印输出本人的图。")
(@:define-config '@pm:my-profession "建施"  "你的专业，用于排图号时默认使用的专业/图别")
(@:define-config '@pm:projects-output "D:\\Output" "本地PDF输出目录")
(@:define-config '@pm:projects-yun "Z:\\Projects" "云共享PDF输出目录")
(@:define-config '@pm:projects-yunpath "Z:\\s设计资料交接明细" "云共享工程文件目录")
(@:define-config '@pm:projects-prefix "D:\\Design" "本地工程文件目录")
(@:define-config '@pm:tuku "D:\\Design\\standard" "本地标准图库文件目录")
(@:define-config '@pm:tuku-yun "Z:\\Design\\standard" "云共享标准图库目录")
;;图框参数
(@:define-config '@pm:tukuang "图框" "用于工程管理的图框块名")
(@:define-config '@pm:tukuang-scale 1.0 "插入图框的默认比例")
(@:define-config '@pm:tukuang-mapsheet "A2" "插入图框的默认图幅")

(@:define-config '@pm:tukuang-base 0  "图框基点位置。0 右下角 1 左下角,控制签名插入。")
(@:define-config '@pm:tukuang-corner  0  "图框角点识别方法。0 查表法， 1 包围盒法")
(@:define-config '@pm:bg-tukuang "图框-变更" "用于工程管理的变更图框块名")
(@:define-config '@pm:fa-tukuang "图框-方案" "用于工程管理的方案图框块名")
(@:define-config '@pm:print-tuming 1 "打印输出的文件名是否带有图名。1 带图名; 0 不带图名; 2 带图名及图幅")
(@:define-config '@pm:prefix-profession 1 "文件前缀带图别。")
(@:define-config '@pm:consistency "建设单位;工程名称;子项名称;日期;阶段;工号;版次"
		 "生成目录前须进行检查的项目，保证各图框中的内容一致。")
(@:define-config '@pm:drawing-number "图号" "图框块的图号属性标签名。一般为图号、图纸编号等")
(@:define-config '@pm:drawing-numbers "图号,图纸编号" "可用的图框块的图号属性标签名。")
(@:define-config '@pm:drawing-number-bits 2 "图号位数，小于位数时前面补0。")
(@:define-config '@pm:drawing-number-prefix "" "图号前缀")
(@:define-config '@pm:drawing-number-order "xY" "图框的排列顺序,用大小写xy表示。y在前面的表示y坐标优先，大写表示从大到小。")
(@:define-config '@pm:drawing-number-sub-order 0 "是否对次级排序进行次级编号。1 是，0 否 。例：前缀-主编号-次编号")
(@:define-config '@pm:drawing-name "图名" "图框块的图名属性标签名。一般为图名、图纸名称等")
(@:define-config '@pm:drawing-names "图名,图纸名称,图名第一行,图名第二行" "可用的图框块的图名属性标签名。")
(@:define-config '@pm:profession-name "专业" "图框块的专业属性标签名。一般为专业、图别等")
;; 目录写图:行距 序号 图号 名称 图号(到起点的位置
(@:define-config '@pm:ml-order-preliminary "建初;结初;水初;暖初;电初" "初步设计图目录专业顺序")
(@:define-config '@pm:ml-order "建施;建筑;结施;结构;水施;给排水;暖施;暖通;电施;电气" "施工图目录专业顺序")
(@:define-config '@pm:ml-order-rf "建防;结防;水防;暖防;电防" "人防目录专业顺序")
(@:define-config '@pm:ml-ruler "800;300;1350;3800;15050" "目录成图的定位信息.行距;各显示项距插入点的水平距。")
(@:define-config '@pm:sign-dwg "my-sign" "个人签名 dwg 文件名,不含.dwg扩展表")
(@:define-config '@pm:sign-offset "-1000,12500" "签名基点相对于图框基点的偏移位置")
(@:define-config '@pm:project-tree "项目名称;工程名称;子项名称" "工程命名的自上而下的层级关系。")
(@:define-config '@pm:merge-pdf 1 "打印完成后将多个单图pdf合并为一个pdf文件。")

;; 图幅表
(setq @:paper-size '(("A2" . (594 420))
		     ("A2+0.25" . (743 420))
		     ("A2+0.5" . (891 420))
		     ("A2+0.75" . (1040 420))
		     ("A2+1" . (1188 420))
		     ("A2+1.25" . (1337 420))
		     ("A2+1.5" . (1485 420))
		     ("A1" . (841 594))
		     ("A1+0.25" . (1052 594))
		     ("A1+0.5" . (1262 594))
		     ("A1+0.75" . (1472 594))
		     ("A1+1" . (1682 594))
		     ("A1+1.25" . (1893 594))
		     ("A0" . (1189 841))
		     ("A0+0.125" . (1338 841))
		     ("A0+0.25" . (1487 841))
		     ("A0+0.375" . (1635 841))
		     ("A0+0.5" . (1784 841))
		     ("A2+1/4" . (743 420))
		     ("A2+1/2" . (891 420))
		     ("A2+3/4" . (1040 420))
		     ("A2+5/4" . (1337 420))
		     ("A2+3/2" . (1485 420))
		     ("A1+1/4" . (1052 594))
		     ("A1+1/2" . (1262 594))
		     ("A1+3/4" . (1472 594))
		     ("A1+5/4" . (1893 594))
		     ("A0+1/4" . (1487 841))
		     ("A0+1/2" . (1784 841))
		     ("A4" . (297 210))
		     ("A3" . (420 297))
		     ("A3+1/4" . (525 297))
		     ("A3+1/2" . (630 297))
		     ("A3+3/4" . (735 297))
		     ("A3+1" . (840 297))
		     ("A3v" . (297 420))
		     ("A4v" . (210 297))
		     ))
(defun @pm:video-turorial ()
  (@:cmd"start" "https://www.ixigua.com/7156231833636733447"))
