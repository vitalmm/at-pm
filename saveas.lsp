(defun @pm:saveas-dxf (file-name file-path ss)
  "保存选中的图素到dxf"
  (if (and (= 'str (type file-name))
	   (/= "" file-name)
	   (vl-file-directory-p file-path)
	   (> (sslength ss) 0))
      (progn
	(push-var nil)
	(setq file-name
	      (string:from-lst
	       (reverse (cdr (reverse (string:to-lst file-name "."))))
	       "."))
	(setvar "cmdecho" 0)
	(setvar "filedia" 0)
	(vl-cmdf "_.dxfout" (strcat file-path "\\" file-name)  "O" ss "" "16" )
	(pop-var)
	T)
      nil
      )
  )
(@:add-menu "工程输出" "局部导DXF" "(@pm:menu-saveas-dxf)")
(defun @pm:menu-saveas-dxf (/ folder ss)
  (@:help "保存选中的图素到同名的 dxf 文件")
  (prompt  "请选要导出dxf文件的图素:")
  (setq ss (ssget))
  (setq folder (system:get-folder "请选要dxf文件输出的目录:"))
  (@pm:saveas-dxf
   (getvar "DWGNAME")
   folder ss))

(defun c:mj-req ()
  (@pm:saveas-dxf
   (getvar "DWGNAME")
   "C:\\2dTO3d"
   (ssget '((-4 . "<NOT") (8 . "DIM,TEMP")(-4 . "NOT>")))))
